#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = 'A Madrid Hold'
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'A')
        self.assertEqual(city, 'Madrid')
        self.assertEqual(action, ['Hold'])

    def test_read_2(self):
        s = 'C Barcelona Move Madrid'
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'C')
        self.assertEqual(city, 'Barcelona')
        self.assertEqual(action, ['Move', 'Madrid'])

    def test_read_3(self):
        s = 'B London Support C'
        army, city, action = diplomacy_read(s)
        self.assertEqual(army,  'B')
        self.assertEqual(city, 'London')
        self.assertEqual(action, ['Support', 'C'])

    # ----
    # solve
    # ----

    def test_eval_1(self):
        instructions = [
            ['A', 'Madrid', ['Hold']],
            ['B', 'Barcelona', ['Move', 'Madrid']],
            ['C', 'London', ['Support', 'B']],
            ['D', 'Austin', ['Move', 'Dallas']]
        ]

        armies = diplomacy_eval(instructions)

        self.assertEqual(armies, [
            ('A', '[dead]'), 
            ('B', 'Madrid'),
            ('C', 'London'),
            ('D', 'Dallas')
        ])

    def test_eval_2(self):
        instructions = [
            ['A', 'Madrid', ['Hold']],
            ['B', 'Barcelona', ['Move', 'Madrid']]
        ]

        armies = diplomacy_eval(instructions)

        self.assertEqual(armies, [
            ('A', '[dead]'), 
            ('B', '[dead]')
        ])

    def test_eval_3(self):
        instructions = [
            ['A', 'Madrid', ['Hold']],
            ['B', 'Barcelona', ['Move', 'Madrid']],
            ['C', 'London', ['Support', 'B']],
            ['D', 'Austin', ['Move', 'London']]
        ]

        armies = diplomacy_eval(instructions)

        self.assertEqual(armies, [
            ('A', '[dead]'), 
            ('B', '[dead]'),
            ('C', '[dead]'),
            ('D', '[dead]')
        ])

    def test_eval_4(self):
        instructions = [
            ['A', 'Madrid', ['Hold']],
            ['B', 'Barcelona', ['Move', 'Madrid']],
            ['C', 'London', ['Move', 'Madrid']]
        ]

        armies = diplomacy_eval(instructions)

        self.assertEqual(armies, [
            ('A', '[dead]'), 
            ('B', '[dead]'),
            ('C', '[dead]')
        ])
    
    def test_eval_5(self):
        instructions = [
            ['A', 'Madrid', ['Hold']],
            ['B', 'Barcelona', ['Move', 'Madrid']],
            ['C', 'London', ['Move', 'Madrid']],
            ['D', 'Paris', ['Support', 'B']]
        ]

        armies = diplomacy_eval(instructions)

        self.assertEqual(armies, [
            ('A', '[dead]'), 
            ('B', 'Madrid'),
            ('C', '[dead]'),
            ('D', 'Paris')
        ])

    def test_eval_6(self):
        instructions = [
            ['A', 'Madrid', ['Hold']],
            ['B', 'Barcelona', ['Move', 'Madrid']],
            ['C', 'London', ['Move', 'Madrid']],
            ['D', 'Paris', ['Support', 'B']],
            ['E', 'Austin', ['Support', 'A']]
        ]

        armies = diplomacy_eval(instructions)

        self.assertEqual(armies, [
            ('A', '[dead]'), 
            ('B', '[dead]'),
            ('C', '[dead]'),
            ('D', 'Paris'),
            ('E', 'Austin')
        ])

    # ----
    # print
    # ----

    def test_print_1(self):
        w = StringIO()

        armies = [
            ('A', '[dead]'), 
            ('B', 'Madrid'),
            ('C', 'London')
        ]

        diplomacy_print(w, armies)

        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

    def test_print_2(self):
        w = StringIO()
        armies= [
            ('A', '[dead]'), 
            ('B', '[dead]')
        ]

        diplomacy_print(w, armies)

        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')


    def test_print_3(self):
        w = StringIO()
        armies= [
            ('A', '[dead]'), 
            ('B', '[dead]'),
            ('C', '[dead]'),
            ('D', 'Paris'),
            ('E', 'Austin')
        ]

        diplomacy_print(w, armies)

        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Madrid\nC London\n')

    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\n')
        
    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()
